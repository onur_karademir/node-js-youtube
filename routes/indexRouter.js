const express = require("express");
const router = express.Router();
const Video = require("../models/videoModel");
const User = require("../models/userModel");
router.get("/", (req, res) => {
  Video.find({}).then((result,err)=> {
    if (err) {
      console.log(err);
    }else{
      if (req.query.video != null && req.query.video !== '') {
        const videoQuery = new RegExp(req.query.video, 'i');
        Video.find({videoName:videoQuery}).then(video=> {
          res.render("index",{user:result,videoQuery:video});
        })
      }else{
        res.render("index",{user:result,videoQuery:null});
      }
    }
  })
});

module.exports = router;
