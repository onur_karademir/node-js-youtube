const express = require("express");

const router = express.Router();
const path = require("path");
const User = require("../models/userModel");
const Video = require("../models/videoModel");
router.get(`/:videoName`,async (req, res) => {
  const videoArray = await Video.find();
  const video = await Video.find({videoName:req.params.videoName})
  console.log("ii",videoArray);
  console.log("jj",video);
  console.log("jj",video.video);
  res.render("video/video-page",{video:video,videoArray:videoArray,resultComment:video});
});
// User.find().then((result, err) => {
//   result.forEach((item) => {
//     item.profileVideo.forEach((video) => {
//       if (err) {
//         console.log(err);
//       } else {
//         router.get(`/:videoName`,(req, res) => {
//           Video.find().then(videoArray=> {
//             Video.find({videoName:video.videoName}).then(resultComment=> {
//               console.log("j",video.videoName);
//               console.log("i",resultComment);
//               res.render("video/video-page", { video: video,videoArray:videoArray,userImage:item.profileImg,resultComment:resultComment});
//             })
//           })
//         });
//       }
//     });
//   });
// });

router.post(`/:name/:videoId`,(req,res)=> {
  var videoName = req.params.name
  const {nick}= req.user
  var objComment = {comment:req.body.comment,commentNick:nick,commentDate:new Date().toLocaleDateString(),commentTime:new Date().toLocaleTimeString()};
  Video.findOneAndUpdate({videoId:req.params.videoId},{$push: {commentArray: objComment}},(err,video)=> {
    console.log(video);
    if (err) {
      console.log(err);
    }else{
      res.redirect(`/video/${videoName}`)
    }
  })
});
router.post(`/:name/:videoId/library`,(req,res)=> {
  var videoName = req.params.name
  var videoId = req.params.videoId
  const {nick}= req.user
  Video.findOne({videoId:videoId}).then(video=> {
    var objLibrary = {video:video.video,postNick:video.postNick,category:video.category,videoName:video.videoName,videoDescription:video.videoDescription,videoId:video.videoId,videoOwnPic:video.videoOwnPic};
    User.findOneAndUpdate({nick:nick},{$push: {library: objLibrary}},(err,user)=> {
      req.session.flashMessage = {
        content : "Video Kitaplığa Eklendi...."
      }
      res.redirect(`/video/${videoName}`)
    })
  })
});
router.post(`/:name/:videoId/subs`,(req,res)=> {
  var videoName = req.params.name
  var videoId = req.params.videoId
  const {nick}= req.user
  Video.findOne({videoId:videoId}).then(video=> {
    var objSubs = {video:video.video,postNick:video.postNick,category:video.category,videoName:video.videoName,videoDescription:video.videoDescription,videoId:video.videoId,videoOwnPic:video.videoOwnPic};
    User.findOneAndUpdate({nick:nick},{$push: {subs: objSubs}},(err,user)=> {
      req.session.flashMessage = {
        content : "kanala üye olundu...."
      }
      res.redirect(`/video/${videoName}`)
    })
  })
});

module.exports = router;
