const express = require("express");
const router = express.Router();
const Video = require("../models/videoModel");
const User = require("../models/userModel");
router.get("/", (req, res) => {
Video.find({}).then(results=> {
    var data = require("../data.json");
    console.log(data);
    res.render("trending/trending",{videos:results,data:data})
})
});

module.exports = router;
