const express = require("express");
const router = express.Router();
const Video = require("../models/videoModel");
const User = require("../models/userModel");
router.get("/", (req, res) => {
  const nick = null;
  if (req.user) {
    const { nick } = req.user;
    User.find({ nick: nick }).then((user) => {
        user.forEach(item=> {
            var arr = item.library
            var unique = arr.filter(function (a) {
                return !this[a.videoName] && (this[a.videoName] = true);
            }, Object.create(null));
            res.render("user/library",{user:unique})
        })
    });
}else {
      res.render("user/library", { user: nick });

  }
});
router.get("/delete/:videoId",(req,res)=> {
    const {nick} = req.user;
    User.update(
        {nick: nick},
        {$pull: {library: {videoId: req.params.videoId}}},
        function(err, result) {
            if (err) {
                console.log(err);
            } else {
                res.redirect("/library")
            }
          }
          )
});

module.exports = router;
