const express = require("express");
const router = express.Router();

router.get("/tv", (req, res) => {
  res.render("youtube/youtubetv");
});

module.exports = router;