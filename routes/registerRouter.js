const express = require("express");
const bcrypt = require('bcryptjs');
const password =require("passport");
const router = express.Router();
const User = require("../models/userModel");
router.get("/register", (req, res) => {
  res.render("user/register");
});
router.post("/register", (req, res) => {
  const { nick, email, password } = req.body;
  let errorMessage = [];
  if (!nick || !email || !password) {
    errorMessage.push({ msg: "Tüm alanları doldurmalısınız..." });
  }
  if (password.length < 6) {
    errorMessage.push({ msg: "Şifre an az 6 karakter olmalıdır..." });
  }
  if (errorMessage.length > 0) {
    res.render("user/register", {
        errorMessage,
        nick,
        email,
        password,
    });
  }else {
      User.findOne({ email: email }).then((user)=>{
          console.log(user);
          if(user && nick){
            errorMessage.push({ msg: "Kullanıcı çoktan atı almış Üsküdar' ı geçmiş..." });
            res.render("user/register", {
                errorMessage,
                nick,
                email,
                password,
            });
          }else{
              var user = new User({
                  nick,
                  email,
                  password
              })
              bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(user.password, salt, (err, hash) => {
                    if (err) throw err;
                    user.password = hash;
                    user.save().then(user => {
                      req.session.flashMessage = {
                        content : "kayıt işlemi başarılı giriş yapabilirsiniz..."
                      }
                        res.redirect('/user/login');
                        console.log(hash);
                        console.log(req.session);
                    })
                      .catch(err => console.log(err));
                  });
              })
          }
      })
  }
});

module.exports = router;
