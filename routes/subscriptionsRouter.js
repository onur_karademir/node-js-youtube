const express = require("express");
const router = express.Router();
const Video = require("../models/videoModel");
const User = require("../models/userModel");
router.get("/", (req, res) => {
    const nick = null;
    if (req.user) {
      const { nick } = req.user;
      User.find({ nick: nick }).then((user) => {
          user.forEach(item=> {
              var arr = item.subs
              var unique = arr.filter(function (a) {
                  return !this[a.postNick] && (this[a.postNick] = true);
              }, Object.create(null));
              res.render("user/subscriptions",{user:unique})
          })
      });
  }else {
        res.render("user/subscriptions", { user: nick });
  
    }
});
router.get("/delete/:name",(req,res)=> {
    const {nick} = req.user;
    User.updateOne(
        {nick: nick},
        {$pull: {subs: {postNick: req.params.name}}},
        function(err, result) {
            if (err) {
                console.log(err);
            } else {
                res.redirect("/subscriptions")
            }
          }
          )
});

module.exports = router;