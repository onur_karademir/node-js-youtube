const express = require("express");

const router = express.Router();
const path = require("path")
const User = require("../models/userModel")
const Video = require("../models/videoModel")

router.get("/profile",(req,res)=> {
  const{ nick } = req.user
  User.find({nick:nick}).then((result,err)=> {
    if (err) {
      console.log(err);
    } else {
      res.render("user/profile",{user:result})
    }
  })
})


router.post("/profile", (req, res) => {
  String.prototype.turkishtoEnglish = function () {
    return this.replace('Ğ','g')
        .replace('Ü','u')
        .replace('Ş','s')
        .replace('I','i')
        .replace('İ','i')
        .replace('Ö','o')
        .replace('Ç','c')
        .replace('ğ','g')
         .replace('ü','u')
        .replace('ş','s')
        .replace('ı','i')
        .replace('ö','o')
        .replace(/\s/g , "-")
        .replace('ç','c');
};
  const {nick} = req.user
  const file = req.files.profileVideo;
  var id =  new Date().getTime()
  file.mv(path.resolve(__dirname,"../public/videos",file.name))
  var videoObj = {video:`../../public/videos/${file.name}`,category:req.body.category,videoName:req.body.videoName.turkishtoEnglish(),videoDescription:req.body.videoDescription,postNick:nick,videoId:id};
  User.findOneAndUpdate({nick:nick},{$push:{profileVideo:videoObj}},(err)=> {
    if (err) {
     console.log(err);
    }else{
      var video = new Video ({
        video:`../../public/videos/${file.name}`,
        postNick:nick,
        category:req.body.category,
        videoName:req.body.videoName.turkishtoEnglish(),
        videoDescription:req.body.videoDescription,
        videoId:id,
        videoOwnPic:req.body.videoOwnPic
      })
      video.save().then(result=> {
        req.session.flashMessage = {
          content : "Video Yükleme İşlemi Başarılı...."
        }
        req.session.video = true;
        res.redirect("/user/profile");
      }).catch(err=> {
        if (err) {
          console.log(err);
        }
      })
    };
  });
   
});
router.post("/profile/image", (req, res) => {
  const file = req.files.profileImage;
  const user = req.user.nick
  file.mv(path.resolve(__dirname,"../public/images/profile-image",file.name))
  var imageObj = {images:`../../public/images/profile-image/${file.name}`};

  User.findOneAndUpdate({nick:user},{profileImg:`../../public/images/profile-image/${file.name}`},(err)=> {
    if (err) {
     console.log(err);
    }else{
     res.redirect("/user/profile")
    };
});
   
  });
router.post("/profile/bgimage", (req, res) => {
  const file = req.files.profileBgImage;
  const user = req.user.nick
  file.mv(path.resolve(__dirname,"../public/images/profile-bg-image",file.name))
  
  User.findOneAndUpdate({nick:user},{profileBgImg:`../../public/images/profile-bg-image/${file.name}`},(err)=> {
    if (err) {
     console.log(err);
    }else{
     res.redirect("/user/profile")
    };
});
   
});
router.get("/profile/delete/:videoId",(req,res)=> {
const {nick} = req.user;
    User.update(
        {nick: nick},
        {$pull: {profileVideo: {videoId: req.params.videoId}}},
        function(err, result) {
            if (err) {
                console.log(err);
            } else {
               Video.findOneAndDelete({videoId:req.params.videoId},function(err,result){
                if (!err) {
                  req.session.flashMessage = {
                    content:"Video kaldırıldı..."
                  }
                  res.redirect("/user/profile")
                }
               })
            }
          }
          )
})

module.exports = router;
