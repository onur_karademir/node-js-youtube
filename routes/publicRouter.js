const express = require("express");
const router = express.Router();
const User = require("../models/userModel")


User.find().then((result, err) => {
    result.forEach((item) => {
        if (err) {
          console.log(err);
        } else {
          router.get(`/${item.nick.replace(/\s/g ,"-")}`, (req, res) => {
            res.render("user/public",{user:item})
          });
        }
    });
  });


module.exports = router;
