const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var newVideo = new Schema({
  video: { type: String, required: true },
  postNick: { type: String, required: true },
  category: { type: String, required: true },
  videoName: { type: String, required: true },
  videoDescription: { type: String, required: true },
  videoId:{type:String},
  videoOwnPic:{type:String},
  commentArray:[{
    comment:String,
    commentNick:String,
    commentDate:String,
    commentTime:String
  }],
});

var Video = mongoose.model("Video", newVideo);

module.exports = Video;
