const mongoose = require("mongoose");
const Schema = mongoose.Schema;


var newUser = new Schema({
    nick:{
        type:String,
        required:true,
        trim:true
    },
    email:{
        type:String,
        required:true,
        trim:true
    },
    password:{
        type:String,
        required:true,
        trim:true
    },
    profileVideo:[
        {videoName:String,video:String,category:String,postNick:String,videoId:String,videoDescription:String}
    ],
    profileImg:{
        type:String,default:"../../public/images/profile-image/user-default123456.png"
    },
    profileBgImg:{
        type:String,default:"../../public/images/profile-bg-image/user-bg-default123456.txt"
    },
    library:[
        
    ],
    subs:[
        
    ]
})

var User = mongoose.model("User",newUser);

module.exports = User;

