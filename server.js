//lib call
const fileUpload = require("express-fileupload")
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require("path");
const express = require("express");
const app = express();
const expressLayouts = require("express-ejs-layouts");
const passport = require("passport");
const flash = require("connect-flash");
const session = require("express-session")
const cookieParser = require('cookie-parser');
require("./config/passport")(passport);

const indexRouter = require("./routes/indexRouter");
const loginRouter = require("./routes/loginRouter");
const logoutRouter = require("./routes/logoutRouter");
const registerRouter = require("./routes/registerRouter");
const profileRouter = require("./routes/profileRouter");
const videoRouter = require("./routes/videoRouter");
const publicRouter = require("./routes/publicRouter");
const trendingRouter = require("./routes/trendingRouter");
const subscriptionsRouter = require("./routes/subscriptionsRouter");
const libraryRouter = require("./routes/libraryRouter");
const historyRouter = require("./routes/historyRouter");
const kidsRouter = require("./routes/kidsRouter");
const tvRouter = require("./routes/tvRouter");

mongoose.connect("mongodb+srv://aidenpearce:Onur050263007@cluster0.bvq9p.mongodb.net/youtube?retryWrites=true&w=majority",
{ useNewUrlParser: true, useUnifiedTopology: true})
.then((db) => console.log("db is connected database..."))
.catch((error) => console.log(error));
//app set
app.set("view engine", "ejs");
//app use
app.use(cookieParser());
app.use(expressLayouts);
app.use("/public", express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(fileUpload());
app.use(flash());
app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(passport.initialize());
app.use(passport.session());
//app router use
app.use("/", (req, res, next) => {
  res.locals.loggedIn = req.isAuthenticated();
  next();
});
app.use("/", (req, res, next) => {
  res.locals.userName = { user: req.user }
  next();
});
app.use((req, res, next) => {
  res.locals.flashMessage = req.session.flashMessage
  delete req.session.flashMessage
  next()
});
app.use((req, res, next) => {
  res.locals.video = req.session.video
  delete req.session.video
  next()
});


app.use("/", indexRouter);
app.use("/user", loginRouter);
app.use("/user", logoutRouter);
app.use("/user", registerRouter);
app.use("/user", profileRouter);
app.use("/user", profileRouter);
app.use("/video", videoRouter);
app.use("/public", publicRouter);
app.use("/trending", trendingRouter);
app.use("/subscriptions", subscriptionsRouter);
app.use("/library", libraryRouter);
app.use("/history", historyRouter);
app.use("/youtube", kidsRouter);
app.use("/youtube", tvRouter);

app.listen(process.env.PORT || 5000);

console.log("local host://5000 ===>>> app start...");
